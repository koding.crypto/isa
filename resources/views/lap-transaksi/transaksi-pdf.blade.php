<!DOCTYPE html>
<html>

<head>
    <title>Data Transaksi</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    <center>
        <h2>Data Transaksi</h2>
    </center>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>ID No</th>
                <th>Brand</th>
                <th>OS</th>
                <th>OS Product Key</th>
                <th>Model</th>
                <th>Serial No</th>
                <th>Processor</th>
                <th>Speed</th>
                <th>RAM</th>
                <th>HDD</th>
                <th>VGA</th>
                <th>Ethernet</th>
                <th>Mac Address</th>
                <th>Weight (kg)</th>
                <th>Optical Drive</th>
                <th>PO No</th>
                <th>Purchase Date</th>
                <th>Supplier Code</th>
                <th>Guarantee (years)</th>
                <th>Remark</th>
                <th>Active Date</th>
            </tr>
        </thead>
        <tbody>
            @if($data_transaksi ?? '' != '')
            @foreach($data_transaksi ?? '' ?? '' as $transaksi)
            <tr>
                <td>{{$transaksi->id}}</td>
                <td>{{$transaksi->idNo}}</td>
                <td>{{$transaksi->brand}}</td>
                <td>{{$transaksi->os}}</td>
                <td>{{$transaksi->os_product_key}}</td>
                <td>{{$transaksi->model}}</td>
                <td>{{$transaksi->serial_no}}</td>
                <td>{{$transaksi->processor}}</td>
                <td>{{$transaksi->speed}}</td>
                <td>{{$transaksi->ram}}</td>
                <td>{{$transaksi->hdd}}</td>
                <td>{{$transaksi->vga}}</td>
                <td>{{$transaksi->ethernet}}</td>
                <td>{{$transaksi->macAddress}}</td>
                <td>{{$transaksi->weight}}</td>
                <td>{{$transaksi->opticalDrive}}</td>
                <td>{{$transaksi->poNo}}</td>
                <td>{{$transaksi->purchaseDate}}</td>
                <td>{{$transaksi->supplierCode}}</td>
                <td>{{$transaksi->guarantee}}</td>
                <td>{{$transaksi->remark}}</td>
                <td>{{$transaksi->activeDate}}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>ID No</th>
                <th>Brand</th>
                <th>OS</th>
                <th>OS Product Key</th>
                <th>Model</th>
                <th>Serial No</th>
                <th>Processor</th>
                <th>Speed</th>
                <th>RAM</th>
                <th>HDD</th>
                <th>VGA</th>
                <th>Ethernet</th>
                <th>Mac Address</th>
                <th>Weight (kg)</th>
                <th>Optical Drive</th>
                <th>PO No</th>
                <th>Purchase Date</th>
                <th>Supplier Code</th>
                <th>Guarantee (years)</th>
                <th>Remark</th>
                <th>Active Date</th>
            </tr>
        </tfoot>
    </table>
</body>

</html>