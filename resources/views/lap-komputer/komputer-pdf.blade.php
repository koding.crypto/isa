<!DOCTYPE html>
<html>

<head>
    <title>Data Komputer</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    <center>
        <h2>Data Komputer</h2>
    </center>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Kode User</th>
                <th>Nama User</th>
                <th>Jabatan</th>
                <th>Kode Departemen</th>
            </tr>
        </thead>
        <tbody>
            @if($data_komputer ?? '' != '')
            @foreach($data_komputer ?? '' ?? '' as $komputer)
            <tr>
                <td>{{$komputer->kodeUser}}</td>
                <td>{{$komputer->namaUser}}</td>
                <td>{{$komputer->jabatan}}</td>
                <td>{{$komputer->kodeDepartemen}}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>Kode User</th>
                <th>Nama User</th>
                <th>Jabatan</th>
                <th>Kode Departemen</th>
            </tr>
        </tfoot>
    </table>
</body>

</html>