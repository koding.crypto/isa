@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Detail Data HDD</h3>

            <!-- <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/hdd" method="post">
                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Id No</label>
                            <input name="idNo" type="text" class="form-control" id="idNo" aria-describedby="ID No" placeholder="ID" value="{{$data_hdd->id}}">
                        </div>
                        <div class="form-group">
                            <label>hdd</label>
                            <input name="idNo" type="text" class="form-control" id="idNo" aria-describedby="ID No" placeholder="ID" value="{{$data_hdd->hdd}}">
                        </div>



                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button> -->
            <a href="/hdd" class="btn btn-primary btn-sm">Kembali</i></a>
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>

@endsection