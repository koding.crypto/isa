@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Data History</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @if(session('sukses'))
                    <div class="alert alert-success" role="alert">
                        {{session('sukses')}}
                    </div>
                    @endif

                    <!-- <h3 class="card-title">DataTable with default features</h3> -->
                    <div class="row">
                        <div class="col-6">
                            <h3>Total History: {{$count_transaksi ?? ''}}</h3>
                        </div>
                        <div class="col-6">
                            <a href="/transaksi/insert" class="btn btn-primary btn-sm float-right">Buat transaksi</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID No</th>
                                <th>Brand</th>
                                <th>OS</th>
                                <th>Processor</th>
                                <th>RAM</th>
                                <th>HDD</th>
                                <th>VGA</th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @if($data_transaksi ?? '' != '')
                            @foreach($data_transaksi ?? '' ?? '' as $transaksi)
                            <tr>
                                <td>{{$transaksi->id}}</td>
                                <td>{{$transaksi->idNo}}</td>
                                <td>{{$transaksi->brand}}</td>
                                <td>{{$transaksi->os}}</td>
                                <td>{{$transaksi->processor}}</td>
                                <td>{{$transaksi->ram}}</td>
                                <td>{{$transaksi->hdd}}</td>
                                <td>{{$transaksi->vga}}</td>
                                <!-- <td>
                                    <a href="/transaksi/{{$transaksi->id}}/create" class="btn btn-primary btn-sm">create</i></a>
                                    <a href="/transaksi/{{$transaksi->id}}/edit" class="btn btn-warning btn-sm">update</i></a>
                                    <a href="/transaksi/{{$transaksi->id}}/delete" class="btn btn-danger btn-sm">delete</i></a>
                                    <a href="/transaksi/{{$transaksi->id}}/tohistory" class="btn btn-secondary btn-sm">to history</i></a>
                                    <a href="/transaksi/{{$transaksi->id}}/read" class="btn btn-success btn-sm">read</i></a>
                                </td> -->
                                <!-- <td>
                                    <a href="/datatransaksi/{{$transaksi->kodetransaksi}}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                    <a href="/datatransaksi/{{$transaksi->kodetransaksi}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin dihapus?')"><i class="fas fa-trash"></i></a></td>
                                </td> -->
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>ID No</th>
                                <th>Brand</th>
                                <th>OS</th>
                                <th>Processor</th>
                                <th>RAM</th>
                                <th>HDD</th>
                                <th>VGA</th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- <p>Link 1</p>
            <a data-toggle="modal" data-id="ISBN564541" title="Add this item" class="open-AddBookDialog btn btn-primary" href="#addBookDialog">test</a>

            <p>&nbsp;</p>


            <p>Link 2</p>
            <a data-toggle="modal" data-id="ISBN-001122" title="Add this item" class="open-AddBookDialog btn btn-primary" href="#addBookDialog">test</a>

            <div class="modal hide" id="addBookDialog">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3>Modal header</h3>
                </div>
                <div class="modal-body">
                    <p>some content</p>
                    <input type="text" name="bookId" id="bookId" value="" />
                </div>
            </div> -->


        </div>
    </div>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <input type="text" name="bookId" id="bookId" value="" /> -->
                <p>Yakin Hapus Data&hellip;?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning">Add to History</button>
                <!-- <button type="button" href="#" class="btn btn-danger">Delete</button> -->
                <a href="#" class="btn btn-danger">Delete</i></a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- jQuery -->
<script src="/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/js/demo.js"></script>

<!-- page script -->
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
    $(document).on("click", ".open-AddBookDialog", function() {
        var myBookId = $(this).data('id');
        $(".modal-body #bookId").val(myBookId);
        // As pointed out in comments, 
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });

    $(document).on("click", ".open-delete", function() {
        var myBookId = $(this).data('id');
        var href = 'clear.php?clear_id=';
        $(".modal-body #bookId").val(myBookId);
        $('.btn-danger', this).attr('href', href);

        // As pointed out in comments, 
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });
</script>



@endsection