@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Detail Data Transaksi</h3>

            <!-- <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/transaksi" method="post">
                {{csrf_field()}}
                <div class="row">
                    <!-- <form action="/transaksi/create" method="post"> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Id No</label>
                            <input name="idNo" type="text" class="form-control" id="idNo" aria-describedby="ID No" placeholder="ID" value="{{$transaksi->id}}">
                        </div>
                        <div class="form-group">
                            <label>Brand</label>
                            <select class="form-control" name="brand" id="brand">
                                @foreach($data_brand ?? '' as $brand)
                                

                                <option @if ($brand->brand == $transaksi->brand)
                                        selected
                                        @endif
                                        value="{{$brand->brand}}">{{$brand->brand}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <input name="type" type="text" class="form-control" id="type" aria-describedby="Type" placeholder="Type" value="{{$transaksi->type}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Operating System</label>
                            <select class="form-control" name="os" id="os">
                                @foreach($data_os ?? '' as $os)
                                {{$os->os}}
                                <option @if ($os->os == $transaksi->os)
                                        selected
                                        @endif
                                        value="{{$os->os}}">{{$os->os}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>OS Product Key</label>
                            <input name="os_product_key" type="text" class="form-control" id="os_product_key" aria-describedby="OS Product Key" placeholder="OS Product Key" value="{{$transaksi->os_product_key}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Model</label>
                            <input name="model" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Model" value="{{$transaksi->model}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Serial No</label>
                            <input name="serial_no" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Serial No" value="{{$transaksi->serial_no}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Processor</label>
                            <select class="form-control" name="processor" id="processor">
                                @foreach($data_processor ?? '' as $processor)
                                <option @if ($processor->processor == $transaksi->processor)
                                        selected
                                        @endif
                                        value="{{$processor->processor}}">{{$processor->processor}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Speed</label>
                            <input name="speed" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Speed" value="{{$transaksi->speed}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">RAM</label>
                            <select class="form-control" name="ram" id="ram">
                                @foreach($data_ram ?? '' as $ram)
                                <option @if ($ram->ram == $transaksi->ram)
                                        selected
                                        @endif
                                        value="{{$ram->ram}}">{{$ram->ram}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">HDD</label>
                            <select class="form-control" name="hdd" id="hdd">
                                @foreach($data_hdd ?? '' as $hdd)
                                <option @if ($hdd->hdd == $transaksi->hdd)
                                        selected
                                        @endif
                                        value="{{$hdd->hdd}}">{{$hdd->hdd}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">VGA</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="vga" value="Share"
                                @if($transaksi->vga == "Share") checked @endif 
                                >
                                <label class="form-check-label">Share</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="vga" value="Dedicated" 
                                @if($transaksi->vga == "Dedicated") checked @endif 
                                >
                                <label class="form-check-label">Dedicated</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ethernet</label>
                            <select class="form-control" name="ethernet" id="ethernet">
                                @foreach($data_ethernet ?? '' as $ethernet)
                                <option @if ($ethernet->ethernet == $transaksi->ethernet)
                                        selected
                                        @endif
                                        value="{{$ethernet->ethernet}}">{{$ethernet->ethernet}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mac Address</label>
                            <input name="macAddress" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Mac Address" value="{{$transaksi->macAddress}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Weight</label>
                            <input name="weight" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Weight" value="{{$transaksi->weight}}">
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Optical Drive</label>
                            <select class="form-control" name="opticalDrive" id="opticalDrive">
                            @foreach($data_opticalDrive ?? '' as $opticalDrive)
                                {{$opticalDrive->opticaoptical_drivelDrive}}
                                <option value="{{$opticalDrive->optical_drive}}">{{$opticalDrive->optical_drive}}</option>

                                <option @if ($opticalDrive->optical_drive == $transaksi->optical_drive)
                                        selected
                                        @endif
                                        value="{{$opticalDrive->optical_drive}}">{{$opticalDrive->optical_drive}}</option>
                                @endforeach

                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">PO No</label>
                            <input name="poNo" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="PO No" value="{{$transaksi->poNo}}">
                        </div>
                        <div class="form-group">
                            <label for="purchaseDate">Purchase Date</label>
                            <input name="purchaseDate" type="text" class="form-control" id="purchaseDate" aria-describedby="Purchase Date" placeholder="Purchase Date" value="{{$transaksi->weight}}">
                        </div>
                        <div class="form-group">
                            <label for="supplierCode">Supplier Code</label>
                            <input name="supplierCode" type="text" class="form-control" id="supplierCode" aria-describedby="Supplier Code" placeholder="Supplier Code" value="{{$transaksi->supplierCode}}">
                        </div>
                        <div class="form-group">
                            <label for="guarantee">Guarantee</label>
                            <div class="input-group" data-target-input="nearest">
                                <input name="guarantee" type="text" class="form-control" id="guarantee" aria-describedby="Guarantee" placeholder="Guarantee" value="{{$transaksi->guarantee}}">
                                <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text">Years</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="remark">Remark</label>
                            <input name="remark" type="text" class="form-control" id="remark" aria-describedby="Remark" placeholder="Remark" value="{{$transaksi->remark}}">
                        </div>
                        <div class="form-group">
                            <label for="activeDate">Active Date</label>
                            <input name="activeDate" type="text" class="form-control" id="activeDate" aria-describedby="Active Date" placeholder="Active Date" value="{{$transaksi->activeDate}}">
                        </div>


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button> -->
            <a href="/transaksi" class="btn btn-primary btn-sm">Kembali</i></a>
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>

@endsection