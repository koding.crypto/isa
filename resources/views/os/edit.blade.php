@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Ubah Data OS</h3>

            <!-- <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/os/{{$data_os->id}}/update" method="post">
                {{csrf_field()}}
                <div class="row">
                    <!-- <form action="/transaksi/create" method="post"> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Id No</label>
                            <input name="id" type="text" class="form-control" id="id" aria-describedby="id" placeholder="id" value="{{$data_os->id}}">
                        </div>
                        <div class="form-group">
                            <label>os</label>
                            <input name="os" type="text" class="form-control" id="os" aria-describedby="os" placeholder="os" value="{{$data_os->os}}">
                        </div>



                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>

@endsection