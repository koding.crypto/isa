@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Ubah Data User</h3>

            <!-- <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/datauser/{{$data_user->kodeUser}}/update" method="post">
                {{csrf_field()}}s
                <div class="row">
                    <!-- <form action="/transaksi/create" method="post"> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kode User</label>
                            <input name="kodeUser" type="text" class="form-control" id="kodeUser" aria-describedby="ID No" placeholder="ID" value="{{$data_user->kodeUser}}">
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input name="namaUser" type="text" class="form-control" id="namaUser" aria-describedby="ID No" placeholder="ID" value="{{$data_user->namaUser}}">
                        </div>
                        <div class="form-group">
                            <label>Jabatan</label>
                            <input name="jabatan" type="text" class="form-control" id="jabatan" aria-describedby="ID No" placeholder="ID" value="{{$data_user->jabatan}}">
                        </div>
                        <div class="form-group">
                            <label>Kode Departemen</label>
                            <input name="kodeDepartemen" type="text" class="form-control" id="kodeDepartemen" aria-describedby="ID No" placeholder="ID" value="{{$data_user->kodeDepartemen}}">
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>

@endsection