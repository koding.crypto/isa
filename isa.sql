-- MariaDB dump 10.17  Distrib 10.4.10-MariaDB, for osx10.14 (x86_64)
--
-- Host: localhost    Database: isa
-- ------------------------------------------------------
-- Server version	10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','admin@mail.com',NULL,'$2y$10$9iJGvr1BmfaRyzHt21a7SeBuEHeOCdRTYp/96XeQ8i006UGpv.vK6',NULL,NULL,NULL);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computer_brand`
--

DROP TABLE IF EXISTS `computer_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer_brand` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer_brand`
--

LOCK TABLES `computer_brand` WRITE;
/*!40000 ALTER TABLE `computer_brand` DISABLE KEYS */;
INSERT INTO `computer_brand` VALUES (1,'Asus',NULL,NULL),(2,'Acer',NULL,NULL),(3,'Lenovo',NULL,NULL);
/*!40000 ALTER TABLE `computer_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computer_category`
--

DROP TABLE IF EXISTS `computer_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer_category`
--

LOCK TABLES `computer_category` WRITE;
/*!40000 ALTER TABLE `computer_category` DISABLE KEYS */;
INSERT INTO `computer_category` VALUES (1,'001','Kategori 1',NULL,NULL),(2,'002','Kategori 2',NULL,NULL),(3,'003','Kategori 3',NULL,NULL);
/*!40000 ALTER TABLE `computer_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computer_info`
--

DROP TABLE IF EXISTS `computer_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operatingSystem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OSProductKey` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serialNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `processor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hdd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opticalDrive` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ethernet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lan` tinyint(1) NOT NULL,
  `modem` tinyint(1) NOT NULL,
  `modemBrand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `screenFilter` tinyint(1) NOT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `keyboardBrand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyboardType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyboardConnectivity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mouse` tinyint(1) NOT NULL,
  `mouseBrand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mouseType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mouseConnection` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseDate` datetime NOT NULL,
  `supplierCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplierName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantee` double(8,2) NOT NULL,
  `guarenteeEndDate` datetime NOT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inputBy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastUpdate` datetime NOT NULL,
  `del` tinyint(1) NOT NULL,
  `activeDate` datetime NOT NULL,
  `fdd` tinyint(1) NOT NULL,
  `poNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `curr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wifi` tinyint(1) NOT NULL,
  `wifiDesc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idno1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inno2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backUp` tinyint(1) NOT NULL,
  `keteranganBackUp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vgaShare` tinyint(1) NOT NULL,
  `vgaSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lanMacAdd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `online` tinyint(1) NOT NULL,
  `dipinjam` tinyint(1) NOT NULL,
  `keteranganPinjam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `underRepair` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer_info`
--

LOCK TABLES `computer_info` WRITE;
/*!40000 ALTER TABLE `computer_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `computer_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daftar_user`
--

DROP TABLE IF EXISTS `daftar_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daftar_user` (
  `kodeUser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaUser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeDepartemen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `daftar_user_kodeuser_unique` (`kodeUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daftar_user`
--

LOCK TABLES `daftar_user` WRITE;
/*!40000 ALTER TABLE `daftar_user` DISABLE KEYS */;
INSERT INTO `daftar_user` VALUES ('001','User 1','Staf','100',NULL,NULL),('002','User 2','Staf','100',NULL,NULL),('003','User 3','Staf','100',NULL,NULL);
/*!40000 ALTER TABLE `daftar_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ethernet`
--

DROP TABLE IF EXISTS `ethernet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ethernet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ethernet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ethernet`
--

LOCK TABLES `ethernet` WRITE;
/*!40000 ALTER TABLE `ethernet` DISABLE KEYS */;
INSERT INTO `ethernet` VALUES (1,'Broadcom',NULL,NULL),(2,'TP-Link',NULL,NULL);
/*!40000 ALTER TABLE `ethernet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hdd`
--

DROP TABLE IF EXISTS `hdd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdd` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hdd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdd`
--

LOCK TABLES `hdd` WRITE;
/*!40000 ALTER TABLE `hdd` DISABLE KEYS */;
INSERT INTO `hdd` VALUES (1,'128 GB',NULL,NULL),(2,'256 GB',NULL,NULL),(3,'512 GB',NULL,NULL),(4,'1 TB',NULL,NULL),(5,'2 TB',NULL,NULL);
/*!40000 ALTER TABLE `hdd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyboard`
--

DROP TABLE IF EXISTS `keyboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keyboard` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `keyboard` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyboard`
--

LOCK TABLES `keyboard` WRITE;
/*!40000 ALTER TABLE `keyboard` DISABLE KEYS */;
INSERT INTO `keyboard` VALUES (1,'Logitech',NULL,NULL),(2,'Razer',NULL,NULL);
/*!40000 ALTER TABLE `keyboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (127,'2014_10_12_000000_create_users_table',1),(128,'2014_10_12_100000_create_password_resets_table',1),(129,'2019_08_19_000000_create_failed_jobs_table',1),(130,'2020_09_14_034240_create_computer_info_table',1),(131,'2020_09_14_034349_create_computer_category_table',1),(132,'2020_09_14_034438_create_computer_brand_table',1),(133,'2020_09_14_034448_create_ram_table',1),(134,'2020_09_14_034657_create_processor_table',1),(135,'2020_09_14_034708_create_hdd_table',1),(136,'2020_09_14_034717_create_daftar_user_table',1),(137,'2020_09_15_062525_create_admin_table',1),(138,'2020_09_23_141345_create_os_table',1),(139,'2020_09_23_145642_create_optical_drive_table',1),(140,'2020_09_23_145656_create_ethernet_table',1),(141,'2020_09_23_145704_create_modem_table',1),(142,'2020_09_23_145711_create_keyboard_table',1),(143,'2020_09_23_145715_create_mouse_table',1),(144,'2020_10_01_151417_create_transaksi_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modem`
--

DROP TABLE IF EXISTS `modem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modem` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modem`
--

LOCK TABLES `modem` WRITE;
/*!40000 ALTER TABLE `modem` DISABLE KEYS */;
INSERT INTO `modem` VALUES (1,'TP-Link',NULL,NULL),(2,'Huawei',NULL,NULL);
/*!40000 ALTER TABLE `modem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mouse`
--

DROP TABLE IF EXISTS `mouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mouse` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mouse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mouse`
--

LOCK TABLES `mouse` WRITE;
/*!40000 ALTER TABLE `mouse` DISABLE KEYS */;
INSERT INTO `mouse` VALUES (1,'Logitech',NULL,NULL),(2,'Razer',NULL,NULL);
/*!40000 ALTER TABLE `mouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optical_drive`
--

DROP TABLE IF EXISTS `optical_drive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optical_drive` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `optical_drive` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optical_drive`
--

LOCK TABLES `optical_drive` WRITE;
/*!40000 ALTER TABLE `optical_drive` DISABLE KEYS */;
INSERT INTO `optical_drive` VALUES (1,'Samsung',NULL,NULL),(2,'LG',NULL,NULL);
/*!40000 ALTER TABLE `optical_drive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `os`
--

DROP TABLE IF EXISTS `os`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `os` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `os`
--

LOCK TABLES `os` WRITE;
/*!40000 ALTER TABLE `os` DISABLE KEYS */;
INSERT INTO `os` VALUES (1,'Windows 8 64-bit',NULL,NULL),(2,'MacOS Catalina',NULL,NULL),(3,'Linux Ubuntu 20.0',NULL,NULL);
/*!40000 ALTER TABLE `os` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processor`
--

DROP TABLE IF EXISTS `processor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `processor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processor`
--

LOCK TABLES `processor` WRITE;
/*!40000 ALTER TABLE `processor` DISABLE KEYS */;
INSERT INTO `processor` VALUES (1,'Intel i3',NULL,NULL),(2,'Intel i5',NULL,NULL),(3,'Intel i9',NULL,NULL);
/*!40000 ALTER TABLE `processor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ram`
--

DROP TABLE IF EXISTS `ram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ram` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ram`
--

LOCK TABLES `ram` WRITE;
/*!40000 ALTER TABLE `ram` DISABLE KEYS */;
INSERT INTO `ram` VALUES (1,'1 GB',NULL,NULL),(2,'2 GB',NULL,NULL),(3,'4 GB',NULL,NULL),(4,'8 GB',NULL,NULL),(5,'16 GB',NULL,NULL),(6,'32 GB',NULL,NULL);
/*!40000 ALTER TABLE `ram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `os_product_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `processor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hdd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ethernet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `macAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opticalDrive` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseDate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplierCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activeDate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyboard` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mouse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi`
--

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
INSERT INTO `transaksi` VALUES (1,'1','Acer','Xyz','Windows 8 64-bit','1234567890','Model 1','9876543210','Intel i3','10 Mbps','8 GB','512 GB','Share','TP-Link','1111-2222-3333-4444','1 kg','Samsung','1111','1 Oktober 2020','1234567890','3','Remark 1','10 Oktober 2020','Logitech','Logitech',0,NULL,NULL),(2,'2','Asus','Abc','Linux Ubuntu 20.0','1234567890','Model 1','9876543210','Intel i3','10 Mbps','8 GB','512 GB','Share','TP-Link','1111-2222-3333-4444','1 kg','Samsung','1111','10 Oktober 2020','1234567890','3','Remark 1','20 Oktober 2020','Logitech','Logitech',0,NULL,NULL),(3,'3','Acer','Cde','Windows 8 64-bit','1234567890','Model 2','9876543210','Intel i5','10 Mbps','8 GB','512 GB','Share','TP-Link','1111-2222-3333-4444','1 kg','Samsung','1111','20 Oktober 2020','1234567890','3','Remark 1','25 Oktober 2020','Logitech','Logitech',0,NULL,NULL);
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-24 18:08:39
