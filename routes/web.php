<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    redirect('/login');
});

Route::get('/login', 'User@login');
Route::get('/login', 'User@login');
Route::post('/loginPost', 'User@loginPost');
Route::get('/logout', 'User@logout');

Route::get('/dashboard', 'User@index');
Route::get('/test', 'User@test');

Route::get('/datauser', 'DaftarUser@index');
Route::post('/datauser/create', 'DaftarUser@create');
Route::get('/datauser/{id}/edit', 'DaftarUser@edit');
Route::post('/datauser/{id}/update', 'DaftarUser@update');
Route::get('/datauser/{id}/delete', 'DaftarUser@delete');
Route::get('/datauser/{id}/read', 'DaftarUser@read');

Route::get('/computer', 'ComputerInfo@index');
Route::post('/computer/create', 'ComputerInfo@create');
Route::get('/computer/{id}/edit', 'ComputerInfo@edit');
Route::post('/computer/{id}/update', 'ComputerInfo@update');
Route::get('/computer/{id}/delete', 'ComputerInfo@delete');
Route::get('/computer/{id}/read', 'ComputerInfo@read');

Route::get('/brand', 'Brand@index');
Route::post('/brand/create', 'Brand@create');
Route::get('/brand/{id}/edit', 'Brand@edit');
Route::post('/brand/{id}/update', 'Brand@update');
Route::get('/brand/{id}/delete', 'Brand@delete');
Route::get('/brand/{id}/read', 'Brand@read');

Route::get('/category', 'Category@index');
Route::post('/category/create', 'Category@create');
Route::get('/category/{id}/edit', 'Category@edit');
Route::post('/category/{id}/update', 'Category@update');
Route::get('/category/{id}/delete', 'Category@delete');
Route::get('/category/{id}/read', 'Category@read');

Route::get('/os', 'Os@index');
Route::post('/os/create', 'Os@create');
Route::get('/os/{id}/edit', 'Os@edit');
Route::post('/os/{id}/update', 'Os@update');
Route::get('/os/{id}/delete', 'Os@delete');
Route::get('/os/{id}/read', 'Os@read');

Route::get('/processor', 'Processor@index');
Route::post('/processor/create', 'Processor@create');
Route::get('/processor/{id}/edit', 'Processor@edit');
Route::post('/processor/{id}/update', 'Processor@update');
Route::get('/processor/{id}/delete', 'Processor@delete');
Route::get('/processor/{id}/read', 'Processor@read');

Route::get('/ram', 'Ram@index');
Route::post('/ram/create', 'Ram@create');
Route::get('/ram/{id}/edit', 'Ram@edit');
Route::post('/ram/{id}/update', 'Ram@update');
Route::get('/ram/{id}/delete', 'Ram@delete');
Route::get('/ram/{id}/read', 'Ram@read');

Route::get('/hdd', 'Hdd@index');
Route::post('/hdd/create', 'Hdd@create');
Route::get('/hdd/{id}/edit', 'Hdd@edit');
Route::post('/hdd/{id}/update', 'Hdd@update');
Route::get('/hdd/{id}/delete', 'Hdd@delete');
Route::get('/hdd/{id}/read', 'Hdd@read');

Route::get('/optical-drive', 'OpticalDrive@index');
Route::post('/optical-drive/create', 'OpticalDrive@create');
Route::get('/optical-drive/{id}/edit', 'OpticalDrive@edit');
Route::post('/optical-drive/{id}/update', 'OpticalDrive@update');
Route::get('/optical-drive/{id}/delete', 'OpticalDrive@delete');
Route::get('/optical-drive/{id}/read', 'OpticalDrive@read');

Route::get('/ethernet', 'Ethernet@index');
Route::post('/ethernet/create', 'Ethernet@create');
Route::get('/ethernet/{id}/edit', 'Ethernet@edit');
Route::post('/ethernet/{id}/update', 'Ethernet@update');
Route::get('/ethernet/{id}/delete', 'Ethernet@delete');
Route::get('/ethernet/{id}/read', 'Ethernet@read');

Route::get('/modem', 'Modem@index');
Route::post('/modem/create', 'Modem@create');
Route::get('/modem/{id}/edit', 'Modem@edit');
Route::post('/modem/{id}/update', 'Modem@update');
Route::get('/modem/{id}/delete', 'Modem@delete');
Route::get('/modem/{id}/read', 'Modem@read');

Route::get('/keyboard', 'Keyboard@index');
Route::post('/keyboard/create', 'Keyboard@create');
Route::get('/keyboard/{id}/edit', 'Keyboard@edit');
Route::post('/keyboard/{id}/update', 'Keyboard@update');
Route::get('/keyboard/{id}/delete', 'Keyboard@delete');
Route::get('/keyboard/{id}/read', 'Keyboard@read');

Route::get('/mouse', 'Mouse@index');
Route::post('/mouse/create', 'Mouse@create');
Route::get('/mouse/{id}/edit', 'Mouse@edit');
Route::post('/mouse/{id}/update', 'Mouse@update');
Route::get('/mouse/{id}/delete', 'Mouse@delete');
Route::get('/mouse/{id}/read', 'Mouse@read');

Route::get('/transaksi', 'Transaksi@index');
Route::get('/transaksi/insert', 'Transaksi@insert');
Route::get('/transaksi/{id}/edit', 'Transaksi@edit');
Route::get('/transaksi/{id}/read', 'Transaksi@read');
Route::post('/transaksi/{id}/update', 'Transaksi@update');
Route::post('/transaksi/create', 'Transaksi@create');
Route::get('/transaksi/{id}/delete', 'Transaksi@delete');
Route::get('/transaksi/{id}/tohistory', 'Transaksi@tohistory');

Route::get('/history', 'History@index');

Route::get('/lap-transaksi', 'LaporanTransaksi@index');
Route::get('/lap-transaksi/pdf', 'LaporanTransaksi@pdf');
Route::get('/lap-transaksi/excel', 'LaporanTransaksi@excel');

Route::get('/lap-komputer', 'LaporanKomputer@index');
Route::get('/lap-komputer/pdf', 'LaporanKomputer@pdf');
Route::get('/lap-komputer/excel', 'LaporanKomputer@excel');
