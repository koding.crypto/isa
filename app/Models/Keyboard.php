<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyboard extends Model
{
    protected  $table = "keyboard";
    public $timestamp = false;

    protected $fillable = [
        'keyboard'
    ];
}
