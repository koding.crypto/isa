<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected  $table = "transaksi";
    public $timestamp = false;

    protected $fillable = [
        'idNo',
        'brand',
        'type',
        'os',
        'os_product_key',
        'model',
        'serial_no',
        'processor',
        'speed',
        'ram',
        'hdd',
        'vga',
        'ethernet',
        'macAddress',
        'weight',
        'opticalDrive',
        'poNo',
        'purchaseDate',
        'supplierCode',
        'guarantee',
        'remark',
        'activeDate',
        'keyboard',
        'mouse',
        'history'
    ];
}
