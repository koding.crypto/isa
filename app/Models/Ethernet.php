<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ethernet extends Model
{
    protected  $table = "ethernet";
    public $timestamp = false;

    protected $fillable = [
        'ethernet'
    ];
}
