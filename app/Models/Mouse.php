<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mouse extends Model
{
    protected  $table = "mouse";
    public $timestamp = false;

    protected $fillable = [
        'mouse'
    ];
}
