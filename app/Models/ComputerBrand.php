<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComputerBrand extends Model
{
    protected  $table = "computer_brand";
    public $timestamp = false;

    protected $fillable = [
        'brand'
    ];
}
