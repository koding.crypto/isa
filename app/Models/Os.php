<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Os extends Model
{
    protected  $table = "os";
    public $timestamp = false;

    protected $fillable = [
        'os'
    ];
}
