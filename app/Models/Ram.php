<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ram extends Model
{
    protected  $table = "ram";
    public $timestamp = false;

    protected $fillable = [
        'ram'
    ];
}
