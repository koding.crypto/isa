<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaftarUser extends Model
{
    protected  $table = "daftar_user";
    public $timestamp = false;

    protected $fillable = [
        'kodeUser',
        'namaUser',
        'jabatan',
        'kodeDepartemen',
        'created_at',
        'updated_at'
    ];
}
