<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modem extends Model
{
    protected  $table = "modem";
    public $timestamp = false;

    protected $fillable = [
        'modem'
    ];
}
