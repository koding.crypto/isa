<?php

namespace App\Exports;

use App\DaftarUser;
use App\Models\DaftarUser as ModelsDaftarUser;
use Maatwebsite\Excel\Concerns\FromCollection;

class KomputerExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return ModelsDaftarUser::all();
    }
}
