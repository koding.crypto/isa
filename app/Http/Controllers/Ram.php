<?php

namespace App\Http\Controllers;

use App\Models\Ram as ModelsRam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Ram extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_ram = ModelsRam::all();
            $count_ram = $data_ram->count();

            return view(
                'ram.index',
                [
                    'data_ram' => $data_ram,
                    'count_ram' => $count_ram,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsRam::create($request->all());
        return redirect('/ram')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_ram = ModelsRam::find($id);

        return view(
            'ram.read',
            [
                'data_ram' => $data_ram,
            ]
        );
    }

    public function delete($id)
    {
        $data_ram = ModelsRam::find($id);
        $data_ram->delete();

        return redirect('/ram')->with('sukses', 'Data berhasil dihapus');
    }
}
