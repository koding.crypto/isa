<?php

namespace App\Http\Controllers;

use App\Models\ComputerBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Brand extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_brand = ComputerBrand::all();
            $count_brand = $data_brand->count();

            return view(
                'brand.index',
                [
                    'data_brand' => $data_brand,
                    'count_brand' => $count_brand
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ComputerBrand::create($request->all());
        return redirect('/brand')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_brand = ComputerBrand::find($id);

        return view(
            'brand.read',
            [
                'data_brand' => $data_brand,
            ]
        );
    }

    public function edit($id)
    {
        $data_brand = ComputerBrand::find($id);

        return view(
            'brand.edit',
            [
                'data_brand' => $data_brand,
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $data_brand = ComputerBrand::find($id);

        $data_brand->update($request->all());

        return redirect('/brand')->with('sukses', 'Data berhasil diperbarui');
    }

    public function delete($id)
    {
        $data_brand = ComputerBrand::find($id);
        $data_brand->delete();

        return redirect('/brand')->with('sukses', 'Data berhasil dihapus');
    }
}
