<?php

namespace App\Http\Controllers;

use App\Models\DaftarUser as ModelsDaftarUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DaftarUser extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_user = ModelsDaftarUser::all();
            $count_user = $data_user->count();

            return view(
                'daftarUser.index',
                [
                    'data_user' => $data_user,
                    'count_user' => $count_user,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsDaftarUser::create($request->all());
        return redirect('/datauser')->with('sukses', 'Data berhasil disimpan');
    }
    public function read($kodeUser)
    {
        $data_user = ModelsDaftarUser::where('kodeUser', $kodeUser)->first();

        return view(
            'daftarUser.read',
            [
                'data_user' => $data_user,
            ]
        );
    }
    public function edit($kodeUser)
    {
        $data_user = ModelsDaftarUser::where('kodeUser', $kodeUser)->first();

        return view(
            'daftarUser.edit',
            [
                'data_user' => $data_user,
            ]
        );
    }

    public function update(Request $request, $kodeUser)
    {
        $data_user = ModelsDaftarUser::where('kodeUser', $kodeUser)->first();

        $data_user->where('kodeUser', $kodeUser)->update(
            [
                'kodeUser' => $request->kodeUser,
                'namaUser' => $request->namaUser,
                'jabatan' => $request->jabatan,
                'kodeDepartemen' => $request->kodeDepartemen,
            ]
        );

        return redirect('/datauser')->with('sukses', 'Data berhasil diperbarui');
        // return dd($request);
    }

    public function delete($id)
    {
        $data_user = ModelsDaftarUser::find($id);
        $data_user->delete();

        return redirect('/datauser')->with('sukses', 'Data berhasil dihapus');
    }
}
