<?php

namespace App\Http\Controllers;

use App\Exports\KomputerExport;
use App\Models\DaftarUser;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class LaporanKomputer extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {

            return view(
                'lap-komputer.index'
            );
        }
    }
    public function pdf()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {

            $data_komputer = DaftarUser::all();

            // $view = (string)View::make('lap-transaksi.transaksi-pdf', ['data_komputer' => $data_komputer]);
            // return PDF::loadHTML($view)->setPaper('f4', 'landscape')->setWarnings(false)->save('myfile.pdf');

            $pdf = PDF::loadview('lap-komputer.komputer-pdf', ['data_komputer' => $data_komputer])->setPaper('f4', 'landscape')->setWarnings(false);
            return $pdf->download('komputer-pdf.pdf');
            // return view('lap-transaksi.transaksi-pdf', ['data_komputer' => $data_komputer]);
        }
    }

    public function excel()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return Excel::download(new KomputerExport, 'transaksi.xlsx');
            // return "excel";
        }
    }
}
