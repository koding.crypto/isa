<?php

namespace App\Http\Controllers;

use App\Models\ComputerCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Category extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_category = ComputerCategory::all();
            $count_category = $data_category->count();

            return view(
                'category.index',
                [
                    'data_category' => $data_category,
                    'count_category' => $count_category,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ComputerCategory::create($request->all());
        return redirect('/category')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_category = ComputerCategory::find($id);

        return view(
            'category.read',
            [
                'data_category' => $data_category,
            ]
        );
    }

    public function delete($id)
    {
        $data_category = ComputerCategory::find($id);
        $data_category->delete();

        return redirect('/category')->with('sukses', 'Data berhasil dihapus');
    }
}
