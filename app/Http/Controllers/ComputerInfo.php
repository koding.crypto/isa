<?php

namespace App\Http\Controllers;

use App\Models\ComputerCategory;
use App\Models\ComputerInfo as ModelsComputerInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ComputerInfo extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_computer = ModelsComputerInfo::all();
            $count_computer = $data_computer->count();

            return view(
                'computer.index',
                [
                    'data_computer' => $data_computer,
                    'count_computer' => $count_computer,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsComputerInfo::create($request->all());
        return redirect('/computer')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_computer = ModelsComputerInfo::find($id);

        return view(
            'computer.read',
            [
                'data_computer' => $data_computer,
            ]
        );
    }

    public function delete($id)
    {
        $data_computer = ModelsComputerInfo::find($id);
        $data_computer->delete();

        return redirect('/computer')->with('sukses', 'Data berhasil dihapus');
    }
}
