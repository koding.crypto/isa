<?php

namespace App\Http\Controllers;

use App\Models\Mouse as ModelsMouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Mouse extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_mouse = ModelsMouse::all();
            $count_mouse = $data_mouse->count();

            return view(
                'mouse.index',
                [
                    'data_mouse' => $data_mouse,
                    'count_mouse' => $count_mouse
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsMouse::create($request->all());
        return redirect('/mouse')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_mouse = ModelsMouse::find($id);

        return view(
            'mouse.read',
            [
                'data_mouse' => $data_mouse,
            ]
        );
    }

    public function edit($id)
    {
        $data_mouse = ModelsMouse::find($id);

        return view(
            'mouse.edit',
            [
                'data_mouse' => $data_mouse,
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $data_mouse = ModelsMouse::find($id);

        $data_mouse->update($request->all());

        return redirect('/mouse')->with('sukses', 'Data berhasil diperbarui');
    }

    public function delete($id)
    {
        $data_mouse = ModelsMouse::find($id);
        $data_mouse->delete();

        return redirect('/mouse')->with('sukses', 'Data berhasil dihapus');
    }
}
