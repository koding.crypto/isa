<?php

namespace App\Http\Controllers;

use App\Models\ComputerBrand;
use App\Models\ComputerCategory;
use App\Models\Ethernet;
use App\Models\Hdd;
use App\Models\OpticalDrive;
use App\Models\Os;
use App\Models\Processor;
use App\Models\Ram;
use App\Models\Transaksi as ModelsTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Transaksi extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            // $data_transaksi = ModelsTransaksi::all();
            $data_transaksi = ModelsTransaksi::where('history', false)->get();
            $count_transaksi = $data_transaksi->count();

            return view(
                'transaksi.index',
                [
                    'data_transaksi' => $data_transaksi,
                    'count_transaksi' => $count_transaksi,
                ]
            );
        }
    }

    public function insert()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_category = ComputerCategory::all();
            $data_brand = ComputerBrand::all();
            $data_os = Os::all();
            $data_processor = Processor::all();
            $data_ram = Ram::all();
            $data_hdd = Hdd::all();
            $data_ethernet = Ethernet::all();
            $data_opticalDrive = OpticalDrive::all();

            return view(
                'transaksi.insert',
                [
                    'data_category' => $data_category,
                    'data_brand' => $data_brand,
                    'data_os' => $data_os,
                    'data_processor' => $data_processor,
                    'data_ram' => $data_ram,
                    'data_hdd' => $data_hdd,
                    'data_ethernet' => $data_ethernet,
                    'data_opticalDrive' => $data_opticalDrive,

                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsTransaksi::create($request->all());
        // return view('transaksi.show', ['request' => $request]);
        return redirect('/transaksi')->with('sukses', 'Data berhasil disimpan');
    }

    public function edit($id)
    {
        $transaksi = ModelsTransaksi::find($id);

        $data_category = ComputerCategory::all();
        $data_brand = ComputerBrand::all();
        $data_os = Os::all();
        $data_processor = Processor::all();
        $data_ram = Ram::all();
        $data_hdd = Hdd::all();
        $data_ethernet = Ethernet::all();
        $data_opticalDrive = OpticalDrive::all();

        return view(
            'transaksi.edit',
            [
                'transaksi' => $transaksi,
                'data_category' => $data_category,
                'data_brand' => $data_brand,
                'data_os' => $data_os,
                'data_processor' => $data_processor,
                'data_ram' => $data_ram,
                'data_hdd' => $data_hdd,
                'data_ethernet' => $data_ethernet,
                'data_opticalDrive' => $data_opticalDrive,
            ]
        );
    }

    public function read($id)
    {
        $transaksi = ModelsTransaksi::find($id);

        $data_category = ComputerCategory::all();
        $data_brand = ComputerBrand::all();
        $data_os = Os::all();
        $data_processor = Processor::all();
        $data_ram = Ram::all();
        $data_hdd = Hdd::all();
        $data_ethernet = Ethernet::all();
        $data_opticalDrive = OpticalDrive::all();

        return view(
            'transaksi.read',
            [
                'transaksi' => $transaksi,
                'data_category' => $data_category,
                'data_brand' => $data_brand,
                'data_os' => $data_os,
                'data_processor' => $data_processor,
                'data_ram' => $data_ram,
                'data_hdd' => $data_hdd,
                'data_ethernet' => $data_ethernet,
                'data_opticalDrive' => $data_opticalDrive,
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $transaksi = ModelsTransaksi::find($id);

        $transaksi->update($request->all());

        return redirect('/transaksi')->with('sukses', 'Data berhasil diperbarui');
    }

    public function delete($id)
    {
        $transaksi = ModelsTransaksi::find($id);
        $transaksi->delete();

        return redirect('/transaksi')->with('sukses', 'Data berhasil dihapus');
    }

    public function tohistory($id)
    {
        $transaksi = ModelsTransaksi::find($id);
        $transaksi->update(['history' => true]);
        // $transaksi->history = true;
        // $transaksi->save();
        // $channel->delete();

        return redirect('/transaksi')->with('sukses', 'Data dipindah ke History');
    }
}
