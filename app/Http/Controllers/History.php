<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\ComputerBrand;
use App\Models\ComputerCategory;
use App\Models\Ethernet;
use App\Models\Hdd;
use App\Models\OpticalDrive;
use App\Models\Os;
use App\Models\Processor;
use App\Models\Ram;
use App\Models\Transaksi as ModelsTransaksi;

class History extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            // $data_transaksi = ModelsTransaksi::all();
            $data_transaksi = ModelsTransaksi::where('history', true)->get();
            $count_transaksi = $data_transaksi->count();

            return view(
                'history.index',
                [
                    'data_transaksi' => $data_transaksi,
                    'count_transaksi' => $count_transaksi,
                ]
            );
        }
    }
}
