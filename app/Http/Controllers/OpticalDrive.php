<?php

namespace App\Http\Controllers;

use App\Models\OpticalDrive as ModelsOpticalDrive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OpticalDrive extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_optical_drive = ModelsOpticalDrive::all();
            $count_optical_drive = $data_optical_drive->count();

            return view(
                'opticalDrive.index',
                [
                    'data_optical_drive' => $data_optical_drive,
                    'count_optical_drive' => $count_optical_drive,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsOpticalDrive::create($request->all());
        return redirect('/optical-drive')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_optical_drive = ModelsOpticalDrive::find($id);

        return view(
            'opticalDrive.read',
            [
                'data_optical_drive' => $data_optical_drive,
            ]
        );
    }

    public function delete($id)
    {
        $data_optical_drive = ModelsOpticalDrive::find($id);
        $data_optical_drive->delete();

        return redirect('/optical-drive')->with('sukses', 'Data berhasil dihapus');
    }
}
