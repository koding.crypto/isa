<?php

namespace App\Http\Controllers;

use App\Models\Modem as ModelsModem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Modem extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_modem = ModelsModem::all();
            $count_modem = $data_modem->count();

            return view(
                'modem.index',
                [
                    'data_modem' => $data_modem,
                    'count_modem' => $count_modem,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsModem::create($request->all());
        return redirect('/modem')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_modem = ModelsModem::find($id);

        return view(
            'modem.read',
            [
                'data_modem' => $data_modem,
            ]
        );
    }

    public function delete($id)
    {
        $data_modem = ModelsModem::find($id);
        $data_modem->delete();

        return redirect('/modem')->with('sukses', 'Data berhasil dihapus');
    }
}
