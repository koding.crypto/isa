<?php

namespace App\Http\Controllers;

use App\Models\Keyboard as ModelsKeyboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Keyboard extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_keyboard = ModelsKeyboard::all();
            $count_keyboard = $data_keyboard->count();

            return view(
                'keyboard.index',
                [
                    'data_keyboard' => $data_keyboard,
                    'count_keyboard' => $count_keyboard
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsKeyboard::create($request->all());
        return redirect('/keyboard')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_keyboard = ModelsKeyboard::find($id);

        return view(
            'keyboard.read',
            [
                'data_keyboard' => $data_keyboard,
            ]
        );
    }

    public function edit($id)
    {
        $data_keyboard = ModelsKeyboard::find($id);

        return view(
            'keyboard.edit',
            [
                'data_keyboard' => $data_keyboard,
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $data_keyboard = ModelsKeyboard::find($id);

        $data_keyboard->update($request->all());

        return redirect('/keyboard')->with('sukses', 'Data berhasil diperbarui');
    }

    public function delete($id)
    {
        $data_keyboard = ModelsKeyboard::find($id);
        $data_keyboard->delete();

        return redirect('/keyboard')->with('sukses', 'Data berhasil dihapus');
    }
}
