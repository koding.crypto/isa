<?php

namespace App\Http\Controllers;

use App\Models\Os as ModelsOs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Os extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_os = ModelsOs::all();
            $count_os = $data_os->count();

            return view(
                'os.index',
                [
                    'data_os' => $data_os,
                    'count_os' => $count_os,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsOs::create($request->all());
        return redirect('/os')->with('sukses', 'Data berhasil disimpan');
    }

    public function read($id)
    {
        $data_os = ModelsOs::find($id);

        return view(
            'os.read',
            [
                'data_os' => $data_os,
            ]
        );
    }

    public function delete($id)
    {
        $data_os = ModelsOs::find($id);
        $data_os->delete();

        return redirect('/os')->with('sukses', 'Data berhasil dihapus');
    }
}
