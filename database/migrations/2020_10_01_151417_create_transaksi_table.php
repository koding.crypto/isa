<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->id();
            $table->string('idNo');
            $table->string('brand');
            $table->string('type');
            $table->string('os');
            $table->string('os_product_key');
            $table->string('model');
            $table->string('serial_no');
            $table->string('processor');
            $table->string('speed');
            $table->string('ram');
            $table->string('hdd');
            $table->string('vga');
            $table->string('ethernet');
            $table->string('macAddress');
            $table->string('weight');
            $table->string('opticalDrive');
            $table->string('poNo');
            $table->string('purchaseDate');
            $table->string('supplierCode');
            $table->string('guarantee');
            $table->string('remark');
            $table->string('activeDate');
            $table->string('keyboard');
            $table->string('mouse');
            $table->boolean('history')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
