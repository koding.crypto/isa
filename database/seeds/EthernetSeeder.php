<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EthernetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ethernet')->insert(
            [
                ['ethernet' => 'Broadcom',],
                ['ethernet' => 'TP-Link',]
            ]
        );
    }
}
