<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('computer_category')->insert([
            [
                'code' => '001',
                'category' => 'Kategori 1',
            ],
            ['code' => '002', 'category' => 'Kategori 2',],
            ['code' => '003', 'category' => 'Kategori 3',]
        ]);
    }
}
