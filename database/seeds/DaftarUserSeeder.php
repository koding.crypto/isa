<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DaftarUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('daftar_user')->insert([
            [
                'kodeUser' => '001',
                'namaUser' => 'User 1',
                'jabatan' => 'Staf',
                'kodeDepartemen' => '100',
            ],
            [
                'kodeUser' => '002',
                'namaUser' => 'User 2',
                'jabatan' => 'Staf',
                'kodeDepartemen' => '100',
            ],
            [
                'kodeUser' => '003',
                'namaUser' => 'User 3',
                'jabatan' => 'Staf',
                'kodeDepartemen' => '100',
            ],
        ]);
    }
}
