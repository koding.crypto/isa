<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modem')->insert(
            [
                ['modem' => 'TP-Link',],
                ['modem' => 'Huawei',]
            ]
        );
    }
}
