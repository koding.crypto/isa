<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('computer_brand')->insert([
            ['brand' => 'Asus',],
            ['brand' => 'Acer',],
            ['brand' => 'Lenovo',]
        ]);
    }
}
