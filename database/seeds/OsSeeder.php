<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('os')->insert(
            [
                ['os' => 'Windows 8 64-bit',],
                ['os' => 'MacOS Catalina',],
                ['os' => 'Linux Ubuntu 20.0',]
            ]

        );
    }
}
