<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaksi')->insert(
            [
                [
                    'idNo' => '1',
                    'brand' => 'Acer',
                    'type' => 'Xyz',
                    'os' => 'Windows 8 64-bit',
                    'os_product_key' => '1234567890',
                    'model' => 'Model 1',
                    'serial_no' => '9876543210',
                    'processor' => 'Intel i3',
                    'speed' => '10 Mbps',
                    'ram' => '8 GB',
                    'hdd' => '512 GB',
                    'vga' => 'Share',
                    'ethernet' => 'TP-Link',
                    'macAddress' => '1111-2222-3333-4444',
                    'weight' => '1 kg',
                    'opticalDrive' => 'Samsung',
                    'poNo' => '1111',
                    'purchaseDate' => '1 Oktober 2020',
                    'supplierCode' => '1234567890',
                    'guarantee' => '3',
                    'remark' => 'Remark 1',
                    'activeDate' => '10 Oktober 2020',
                    'keyboard' => 'Logitech',
                    'mouse' => 'Logitech',
                ],
                [
                    'idNo' => '2',
                    'brand' => 'Asus',
                    'type' => 'Abc',
                    'os' => 'Linux Ubuntu 20.0',
                    'os_product_key' => '1234567890',
                    'model' => 'Model 1',
                    'serial_no' => '9876543210',
                    'processor' => 'Intel i3',
                    'speed' => '10 Mbps',
                    'ram' => '8 GB',
                    'hdd' => '512 GB',
                    'vga' => 'Share',
                    'ethernet' => 'TP-Link',
                    'macAddress' => '1111-2222-3333-4444',
                    'weight' => '1 kg',
                    'opticalDrive' => 'Samsung',
                    'poNo' => '1111',
                    'purchaseDate' => '10 Oktober 2020',
                    'supplierCode' => '1234567890',
                    'guarantee' => '3',
                    'remark' => 'Remark 1',
                    'activeDate' => '20 Oktober 2020',
                    'keyboard' => 'Logitech',
                    'mouse' => 'Logitech',
                ],
                [
                    'idNo' => '3',
                    'brand' => 'Acer',
                    'type' => 'Cde',
                    'os' => 'Windows 8 64-bit',
                    'os_product_key' => '1234567890',
                    'model' => 'Model 2',
                    'serial_no' => '9876543210',
                    'processor' => 'Intel i5',
                    'speed' => '10 Mbps',
                    'ram' => '8 GB',
                    'hdd' => '512 GB',
                    'vga' => 'Share',
                    'ethernet' => 'TP-Link',
                    'macAddress' => '1111-2222-3333-4444',
                    'weight' => '1 kg',
                    'opticalDrive' => 'Samsung',
                    'poNo' => '1111',
                    'purchaseDate' => '20 Oktober 2020',
                    'supplierCode' => '1234567890',
                    'guarantee' => '3',
                    'remark' => 'Remark 1',
                    'activeDate' => '25 Oktober 2020',
                    'keyboard' => 'Logitech',
                    'mouse' => 'Logitech',
                ],
            ]

        );
    }
}
