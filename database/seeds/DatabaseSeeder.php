<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(DaftarUserSeeder::class);
        $this->call(EthernetSeeder::class);
        $this->call(HddSeeder::class);
        $this->call(KeyboardSeeder::class);
        $this->call(ModemSeeder::class);
        $this->call(MouseSeeder::class);
        $this->call(OpticalDriveSeeder::class);
        $this->call(OsSeeder::class);
        $this->call(ProcessorSeeder::class);
        $this->call(RamSeeder::class);
        $this->call(TransaksiSeeder::class);
    }
}
