<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ram')->insert(
            [
                ['ram' => '1 GB',],
                ['ram' => '2 GB',],
                ['ram' => '4 GB',],
                ['ram' => '8 GB',],
                ['ram' => '16 GB',],
                ['ram' => '32 GB',]
            ]
        );
    }
}
