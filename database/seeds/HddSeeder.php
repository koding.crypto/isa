<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hdd')->insert(
            [
                ['hdd' => '128 GB',],
                ['hdd' => '256 GB',],
                ['hdd' => '512 GB',],
                ['hdd' => '1 TB',],
                ['hdd' => '2 TB',]
            ]
        );
    }
}
