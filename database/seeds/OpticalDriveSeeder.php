<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpticalDriveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('optical_drive')->insert(
            [
                ['optical_drive' => 'Samsung',],
                ['optical_drive' => 'LG',]
            ]
        );
    }
}
