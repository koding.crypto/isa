<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProcessorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processor')->insert(
            [
                ['processor' => 'Intel i3',],
                ['processor' => 'Intel i5',],
                ['processor' => 'Intel i9',]
            ]

        );
    }
}
